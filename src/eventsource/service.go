package eventsource

import (
	"log"
	"net"
	"eventutils"
	"strings"
	"fmt"
)

type Service struct {
	 port int
	 eventProcessor eventutils.EventProcessor
}

func NewService(port int, eventProcessor eventutils.EventProcessor) Service {
	return Service{port, eventProcessor}
}
 
func (s *Service) Start() {
	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", s.port))
	if err != nil {
		log.Printf("Could not listen ot port %d\n", s.port)
		return
	}
	log.Printf("Listening for events on port %d\n", s.port)
	for {
		client, err := ln.Accept()
		if err != nil {
			log.Println("Could not accept client")
			continue
		}
		s.handleConnection(client)
	}
}

func (s *Service) handleConnection(client net.Conn) {
	var unprocessed string = "";
	var buf [512]byte
	for {
		n, err := client.Read(buf[0:])
		if err != nil {
			return
		}
		unprocessed += string(buf[0:n])
		unprocessed = s.parseEvents(unprocessed)
	}
}

func (s *Service) parseEvents(message string) string {
	parts := strings.Split(message, "\n")
	lastIndex := len(parts) - 1 
	
	if strings.HasSuffix(message, "\n") {
		message = ""
	} else {
		message = parts[lastIndex]
	}
	
	for i := 0; i < lastIndex; i++ {
	   	s.eventProcessor.Push(parts[i])
	}
	return message
}

/*func (s *Service) Test(messages []string) {
	for i := 0; i < len(messages); i++ {
	   	s.eventProcessor.Push(messages[i])
	}
}*/
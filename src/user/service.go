package user

import (
	"log"
	"net"
	"register"
	"fmt"
)

type Service struct {
	 port int
	 registerService register.RegisterService
}

func NewService(port int, registerService register.RegisterService) Service {
	return Service{port, registerService}
}

func (s Service) Start() {
	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", s.port))
	if err != nil {
		log.Printf("Could not listen ot port %d\n", s.port)
		return
	}
	log.Printf("Listening for users on port %d\n", s.port)
	for {
		client, err := ln.Accept()
		if err != nil {
			log.Println("Could not accept client")
			continue
		}
		go handleConnection(client, s.registerService)
	}
}

func handleConnection(client net.Conn, registerService register.RegisterService) {
	var buf [1024]byte
	n, err := client.Read(buf[0:])

	if err != nil {
		return
	}
	
	userID := ""
	if n > 1 {
		userID = string(buf[0:n-1])
		registerService.Register(userID, client)
	}
	
	for {
		n, err := client.Read(buf[0:])
		if err != nil {
			registerService.Unregister(userID)
			return
		}
		log.Printf("Unexpected message from %s %s", userID , string(buf[0:n]))
	}
}
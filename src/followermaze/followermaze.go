package main 

import (
	"container/heap"
	"eventsource"
	"register"
	"user"
	"eventutils"
	"log"
)

var (
	userService user.Service
	eventSourceService eventsource.Service
)

func main() {
	InitServices()    
	
	log.Println("Starting server")
	go userService.Start() 
	eventSourceService.Start() 
}

func InitServices() {
	userServicePort := 9099
	eventSourceServicePort := 9090
	registerService := register.NewRegisterService()
	eventQueue := eventutils.EventPQ{}
    heap.Init(&eventQueue)
	eventProcessor := eventutils.NewEventProcessor(1, eventQueue, registerService)
	userService = user.NewService(userServicePort, registerService)
	eventSourceService = eventsource.NewService(eventSourceServicePort, eventProcessor)
}
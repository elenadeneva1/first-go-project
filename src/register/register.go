package register

import (
	"net"
	"sync"
)

type RegisterService struct {
	users map[string]net.Conn
	usersMutex sync.Mutex
	followers map[string]map[string]bool
	followersMutex sync.Mutex
}

func NewRegisterService() RegisterService {
	return RegisterService{make(map[string]net.Conn), sync.Mutex{}, make(map[string]map[string]bool),  sync.Mutex{}}
}

func (rs RegisterService) Register(user string, connection net.Conn) {
	rs.usersMutex.Lock();
	rs.users[user] = connection
	rs.usersMutex.Unlock();
}

func (rs RegisterService) Unregister(user string) {
	rs.usersMutex.Lock();
	delete(rs.users, user)
	rs.usersMutex.Unlock();
}

func (rs RegisterService) SendMessage(message string, user string) {
	rs.usersMutex.Lock()
	connection, ok := rs.users[user]
	if ok {
		connection.Write([]byte(message + "\n"))
	}
	rs.usersMutex.Unlock()
}

func (rs RegisterService) Broadcast(message string) {
	rs.usersMutex.Lock()
	for _, connection := range rs.users {
    	connection.Write([]byte(message + "\n"))
	}
	rs.usersMutex.Unlock()
}

func (rs RegisterService) Follow(message string, fromUser string, toUser string) {
	rs.followersMutex.Lock()
	followers, ok := rs.followers[toUser]
	if !ok {
		followers = make(map[string]bool)
		rs.followers[toUser] = followers
	}
	followers[fromUser] = true
	rs.SendMessage(message, toUser)
	rs.followersMutex.Unlock()
}

func (rs RegisterService) Unfollow(fromUser string, toUser string) {
	rs.followersMutex.Lock()
	followers, ok := rs.followers[toUser]
	if ok {
		delete(followers, fromUser)
	}
	rs.followersMutex.Unlock()
}

func (rs RegisterService) Status(message string, followedUser string) {
	rs.followersMutex.Lock()
	followers, ok := rs.followers[followedUser]
	if !ok {
		return
	}
	for user, _ := range followers {
    	rs.SendMessage(message, user)
    }
	rs.followersMutex.Unlock()
}
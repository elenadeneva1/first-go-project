package eventutils

type EventPQ []Event

func (pq EventPQ) Len() int {
	return len(pq)
}

func (pq EventPQ) Less(i, j int) bool {
    return pq[i].Seq() < pq[j].Seq()
}

func (pq EventPQ) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *EventPQ) Push(event interface{}) { 
	*pq = append(*pq, event.(Event)) 
}

func (pq *EventPQ) Pop() (item interface{}) {
    item = (*pq)[len(*pq)-1]
    *pq = (*pq)[:len(*pq)-1]
    return item
}
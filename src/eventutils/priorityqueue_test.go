package eventutils

import (
	"testing"
	"container/heap"
)

func TestPriorityQueue(t *testing.T) {
	const msg = "1|S|3"
    event1 := Event{"1|S|3", 1, STATUS, "3", ""} 
    event2 := Event{"12|S|3", 12, STATUS, "3", ""}
    event3 := Event{"123|S|3", 123, STATUS, "3", ""}
    
    eventQueue := EventPQ{}
    heap.Init(&eventQueue)
    
    heap.Push(&eventQueue, event2)
    heap.Push(&eventQueue, event3)
    heap.Push(&eventQueue, event1)
    
    x := heap.Pop(&eventQueue)
    if x != event1 || len(eventQueue) != 2 {
		t.Errorf("Pop  = %v, want %v", x, event1)
	}
    x = heap.Pop(&eventQueue)
    if x != event2 || len(eventQueue) != 1 {
		t.Errorf("Pop  = %v, want %v", x, event2)
	}
    x = heap.Pop(&eventQueue)
	if x != event3 || len(eventQueue) != 0 {
		t.Errorf("Pop  = %v, want %v", x, event3)
	}
}
package eventutils

import "testing"

func TestCreateStatus(t *testing.T) {
	const msg = "1|S|3"
    out := Event{"1|S|3", 1, STATUS, "3", ""} 
    x, e := Create(msg)
	if x != out || e != nil {
		t.Errorf("Create(%v) = %v, want %v", msg, x, out)
	}
}

func TestCreateUnfollow(t *testing.T) {
	const msg = "12|U|2|3"
    out := Event{"12|U|2|3", 12, UNFOLLOW, "2", "3"} 
   	x, e := Create(msg)
	if x != out || e != nil {
		t.Errorf("Create(%v) = %v, want %v", msg, x, out)
	}
}

func TestCreateFollow(t *testing.T) {
	const msg = "12|F|21|3"
    out := Event{"12|F|21|3", 12, FOLLOW, "21", "3"} 
    x, e := Create(msg)
	if x != out || e != nil {
		t.Errorf("Create(%v) = %v, want %v", msg, x, out)
	}
}

func TestCreatePrivate(t *testing.T) {
	const msg = "12|P|21|32"
    out := Event{"12|P|21|32", 12, PRIVATE, "21", "32"} 
    x, e := Create(msg)
	if x != out || e != nil {
		t.Errorf("Create(%v) = %v, want %v", msg, x, out)
	}
}

func TestCreateBroadcast(t *testing.T) {
	const msg = "12|B"
    out := Event{"12|B", 12, BROADCAST, "", ""} 
    x, e := Create(msg)
	if x != out || e != nil {
		t.Errorf("Create(%v) = %v, want %v", msg, x, out)
	}
}

func TestCreateInvalidStatus1(t *testing.T) {
	const msg = "1|S"
    x, e := Create(msg)
	if e == nil {
		t.Errorf("Create(%v) = %v", msg, x)
	}
}

func TestCreateInvalidStatus2(t *testing.T) {
	const msg = "1|S|2|3"
    x, e := Create(msg)
	if e == nil {
		t.Errorf("Create(%v) = %v", msg, x)
	}
}

func TestCreateInvalidUnfollow1(t *testing.T) {
	const msg = "12|U|2|3|23"
    x, e := Create(msg)
	if e == nil {
		t.Errorf("Create(%v) = %v", msg, x)
	}
}

func TestCreateInvalidUnfollow2(t *testing.T) {
	const msg = "12|U|2"
    x, e := Create(msg)
	if e == nil {
		t.Errorf("Create(%v) = %v", msg, x)
	}
}
func TestCreateInvalidFollow1(t *testing.T) {
	const msg = "12|F|2|3|23"
    x, e := Create(msg)
	if e == nil {
		t.Errorf("Create(%v) = %v", msg, x)
	}
}

func TestCreateInvalidFollow2(t *testing.T) {
	const msg = "12|F|2"
    x, e := Create(msg)
	if e == nil {
		t.Errorf("Create(%v) = %v", msg, x)
	}
}


func TestCreateInvalidPrivate1(t *testing.T) {
	const msg = "12|P|2|3|23"
    x, e := Create(msg)
	if e == nil {
		t.Errorf("Create(%v) = %v", msg, x)
	}
}

func TestCreateInvalidPrivate2(t *testing.T) {
	const msg = "12|P|2"
    x, e := Create(msg)
	if e == nil {
		t.Errorf("Create(%v) = %v", msg, x)
	}
}

func TestCreateInvalidBroadcast1(t *testing.T) {
	const msg = "12|B|2|3|23"
    x, e := Create(msg)
	if e == nil {
		t.Errorf("Create(%v) = %v", msg, x)
	}
}

func TestCreateInvalidBroadcast2(t *testing.T) {
	const msg = "12|B|2"
    x, e := Create(msg)
	if e == nil {
		t.Errorf("Create(%v) = %v", msg, x)
	}
}

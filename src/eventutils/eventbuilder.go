package eventutils

import (
	"strings"
	"strconv"
	"errors"
)

func Create(message string) (e Event, err error) {
	fakeEvent := Event{message, 0, NONE, "", ""}
	parts := strings.Split(message, "|")
		
	for i, val := range parts {
  		parts[i] = strings.Trim(val, "|")
	}
	
	if (len(parts) < 2) {
		return fakeEvent, errors.New("Invalid event");
	}
	
	t := StringToType(parts[1])
	if (t == NONE) {
		return fakeEvent, errors.New("Invalid type NONE " + parts[1]);
	}
	
	err = Validate(len(parts), t)
	if err != nil {
		return fakeEvent, err
	}
	
	seq, err := strconv.Atoi(parts[0])
	if err != nil { 
		return fakeEvent, err
	}
	
	switch t {
	case FOLLOW, UNFOLLOW, PRIVATE: 
		return Event{message, seq, t, parts[2], parts[3]}, nil
	case STATUS:
		return Event{message, seq, t, parts[2], ""}, nil
	case BROADCAST:
		return Event{message, seq, t, "", ""}, nil
	}
	return fakeEvent, errors.New("Invalid type " + parts[1])
}
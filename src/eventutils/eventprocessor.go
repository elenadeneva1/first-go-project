package eventutils

import (
	"container/heap"
	"register"
	"log"
)

type EventProcessor struct {
	next int 
	eventQueue EventPQ
	registerService register.RegisterService
}

func NewEventProcessor(next int, eventQueue EventPQ, registerService register.RegisterService) EventProcessor {
	return EventProcessor{next, eventQueue, registerService}
}

func (ep *EventProcessor) Push(eventAsStr string) {
	event, err := Create(eventAsStr)
	if (err != nil) { 
		log.Println("Cannot create event from " + eventAsStr)
		return
	}
	heap.Push(&ep.eventQueue, event)
	ep.processEvents()
}

func (ep *EventProcessor) processEvents() {
	pq := ep.eventQueue
	next := ep.next
	for ;len(pq) > 0 && pq[0].Seq() == next; {
		next++
		event := heap.Pop(&pq).(Event)
		ep.processEvent(event)
	}
	ep.next = next
	ep.eventQueue = pq
}

func (pq *EventProcessor) processEvent(event Event) {
	switch event.EventType() {
		case BROADCAST:
			//log.Printf("Broadcast event %v\n", event);  
			pq.registerService.Broadcast(event.Message())
			return
		case FOLLOW:
			//log.Printf("Follow event %v\n", event); 
			pq.registerService.Follow(event.Message(), event.FromUserID(), event.ToUserID())
			return
		case PRIVATE: 
			//log.Printf("Private event %v\n", event); 
			pq.registerService.SendMessage(event.Message(), event.ToUserID())
			return
		case STATUS:
			//log.Printf("Status event %v\n", event); 
			pq.registerService.Status(event.Message(), event.FromUserID())
			return
		case UNFOLLOW:
			//log.Printf("Unfollow event %v\n", event); 
			pq.registerService.Unfollow(event.FromUserID(), event.ToUserID())
			return
	}
	log.Println("Wrong type " + event.EventType())
}
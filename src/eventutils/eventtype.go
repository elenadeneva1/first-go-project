package eventutils

import (
	"errors"
)

type EventType string

const (
	NONE EventType = "N"
	FOLLOW EventType = "F"
	UNFOLLOW EventType = "U"
	BROADCAST EventType = "B"
	PRIVATE EventType = "P"
	STATUS EventType = "S"
)

func StringToType(s string) (t EventType) {
	switch s {
	case "F": return FOLLOW
	case "U": return UNFOLLOW
	case "B": return BROADCAST
	case "P": return PRIVATE
	case "S": return STATUS 
	} 
    return NONE
}

func Validate(size int, t EventType) (e error) {
	switch t {
	case FOLLOW, UNFOLLOW, PRIVATE:
		if size == 4 {
			return nil
		}
	case BROADCAST:
		if size == 2 {
			return nil
		}
	case STATUS:
		if size == 3 {
			return nil
		}
	}
	return errors.New("Invalid event") 
}
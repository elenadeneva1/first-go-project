package eventutils

type Event struct { 
	message string
	seq int
	eventType EventType
	fromUserID string
	toUserID string
}

func (e Event) Message() string {
	return e.message
}

func (e Event) Seq() int {
	return e.seq
}

func (e Event) EventType() EventType {
	return e.eventType
}

func (e Event) ToUserID() string {
	return e.toUserID
}

func (e Event) FromUserID() string {
	return e.fromUserID
}